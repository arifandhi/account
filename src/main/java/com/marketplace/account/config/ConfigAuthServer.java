package com.marketplace.account.config;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;

import javax.sql.DataSource;

@Configuration
@EnableAuthorizationServer
public class ConfigAuthServer extends AuthorizationServerConfigurerAdapter {

    @Autowired
    @Qualifier("authenticationManagerBean")
    private AuthenticationManager authenticationManager;

    @Autowired
    @Qualifier("userDetailsServiceBean")
    private UserDetailsService userDetailsService;

    @Autowired private DataSource dataSource;

    @Autowired private PasswordEncoder passwordEncoder;

    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.jdbc(dataSource).passwordEncoder(passwordEncoder);
    }

    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security.passwordEncoder(passwordEncoder)
                .checkTokenAccess("hasAuthority('APLIKASI_CLIENT_OAUTH2')")
                .tokenKeyAccess("permitAll()");
    }

    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        endpoints
                .authenticationManager(authenticationManager).userDetailsService(userDetailsService);
    }
    // @Override
    //     public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
    //         clients
    //                 .inMemory()
    //                 .withClient("clientauthcode")
    //                     .secret("123456")
    //                     .authorizedGrantTypes("authorization_code", "refresh_token")
    //                     .scopes("read", "write")
    //                     .resourceIds(RESOURCE_ID)
    //                 .and()
    //                 .withClient("clientcred")
    //                     .secret("123456")
    //                     .authorizedGrantTypes("client_credentials")
    //                     .scopes("trust")
    //                     .resourceIds(RESOURCE_ID)
    //                 .and()
    //                 .withClient("clientapp")
    //                     .secret("123456")
    //                     .authorizedGrantTypes("password")
    //                     .scopes("read", "write")
    //                     .resourceIds(RESOURCE_ID)
    //                 .and()
    //                 .withClient("jsclient")
    //                     .secret("jspasswd")
    //                     .authorizedGrantTypes("implicit")
    //                     .scopes("read", "write")
    //                     .resourceIds(RESOURCE_ID)
    //                     .authorities("CLIENT")
    //                     .redirectUris("http://localhost:20000/implicit-client/")
    //                     .accessTokenValiditySeconds(60 * 60 * 24) // token berlaku seharian, besok harus login ulang
    //                     .autoApprove(true)
    //                 ;
    //     }
}